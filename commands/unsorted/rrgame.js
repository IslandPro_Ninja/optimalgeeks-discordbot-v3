// const discord = require("discord.js");
// const client = new discord.Client();
// const fs = require("fs");
// let config = JSON.parse(fs.readFileSync("./config.json", "utf8"));
// var prefix = config.prefix;
// var token = config.token;
// client.login(token)

// client.on('ready', (message) => {
//     console.log(`rrgame.js is now loaded!`)
// });

// client.on('error', console.error); 

// function sleep(ms) {
//     return new Promise(resolve => setTimeout(resolve, ms));
//   }

// client.on('message', msg => {
            
//     let args = msg.content.substring(prefix.length).split(" ");

//     switch(args[0]) {

//         case 'rolereaction':
//             if(args[1] === "menu") {
//                 if(args[2] === "game") {
//                     if(msg.member.roles.find(n => n.name === "Sr - Developer")) {
//                         msg.delete()
//                         const embed = new discord.RichEmbed()
//                         .setColor(0x008040)
//                         .setDescription(`
//                         **Role Menu: Games**
//                         React to the emoji that matches the role you wish to receive.
                
//                         If you would like to remove the role, simply remove your reaction!
                        
//                         <:LeagueOfLegends:629885990642909184> <@&631594692626350091>
//                         <:minecraft:590733894597214218> <@&631594631511015444>
//                         <:csgo:590735635342557224> <@&631594776407310359>
//                         <:destiny2:631980944592470016> <@&631594989570490369>
//                         <:ark:590734092056657961> <@&631594922864279580>
//                         <:rainbowsixsiege:590735177504915486> <@&631594820833640459>
//                         <:atlas:634069975740448768> <@&634068988787294218>
//                         <:fortnite:634223792486350848> <@&634204131816374292>
//                         <:arma3:637867839822364673> <@&637867665603428362>
//                         <:deadbydaylight:652047186657542155> <@&638239297543208971>
//                         <:halomasterchiefcollection:652346795372052510> <@&652345983337889812>
//                         <:modernwarfare:662115779726016530> <@&662112474883031040> 
//                         <:dauntless:662895709481205790> <@&662771704635981855>           
//                         `)
//                         msg.channel.send(embed).then(function (message) {
//                             message.react('629885990642909184').then(() => {
//                                 sleep(1000).then(() => {
//                                     message.react('590733894597214218').then(() => {
//                                         sleep(1000).then(() => {
//                                             message.react('590735635342557224').then(() => {
//                                                 sleep(1000).then(() => {
//                                                     message.react('631980944592470016').then(() => {
//                                                         sleep(1000).then(() => {
//                                                             message.react('590734092056657961').then(() => {
//                                                                 sleep(1000).then(() => {
//                                                                             message.react('590735177504915486').then(() => {
//                                                                                 sleep(1000).then(() => {
//                                                                                     message.react('634069975740448768').then(() => {
//                                                                                         sleep(1000).then(() => {
//                                                                                             message.react('634223792486350848').then(() => {
//                                                                                                 sleep(1000).then(() => {
//                                                                                                     message.react('637867839822364673').then(() => {
//                                                                                                         sleep(1000).then(() => {
//                                                                                                             message.react('652047186657542155').then(() => {
//                                                                                                               sleep(1000).then(() => {
//                                                                                                                 message.react('652346795372052510').then(() => {
//                                                                                                                     sleep(1000).then(() => {
//                                                                                                                         message.react('662115779726016530').then(() => {
//                                                                                                                             sleep(1000).then(() => {
//                                                                                                                                 message.react('662895709481205790')
//                                                                                                                             })
//                                                                                                                         })
//                                                                                                                     })
//                                                                                                                 })                                        
//                                                                                                             })
//                                                                                                         })
//                                                                                                         })
//                                                                                                     })
//                                                                                                 })
//                                                                                             })
//                                                                                         })
//                                                                                     })
//                                                                                 })
//                                                                             })
//                                                                         })
//                                                                     })
//                                                                 })
//                                                             })
//                                                         })
//                                                     })
//                                                 })
//                                             })
//                                         })
//                                     })
//                                 })
//                             }
//                 }
//             }

//     }
// })

// client.on('ready', () => {
//     client.guilds.find(n => n.name === "Optimal Geeks").channels.find(n => n.name === "game-roles").fetchMessages({ limit: 1});
// });

// client.on('messageReactionAdd', (reaction, user) => {
//     let message = reaction.message, emoji = reaction.emoji;
//     if(user.bot) return;
//     if (emoji.id == '629885990642909184') {
//         message.guild.fetchMember(user.id).then(member => {
//             let LeagueOfLegends = message.guild.roles.find(n => n.name === "League of Legends")
//             if(member.roles.has(LeagueOfLegends.id)) {
//                 member.removeRole('631594692626350091')
//                 member.send("You have been remove from the **League Of Legends** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(LeagueOfLegends.id)) {
//                 member.addRole('631594692626350091')
//                 member.send("You have been added to the **League Of Legends** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "590733894597214218") {
//         message.guild.fetchMember(user.id).then(member => {
//             let Minecraft = message.guild.roles.find(n => n.name === "Minecraft")
//             if(member.roles.has(Minecraft.id)) {
//                 member.removeRole('631594631511015444')
//                 member.send("You have been removed from the **Minecraft** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(Minecraft.id)) {
//                 member.addRole('631594631511015444')
//                 member.send("You have been added to the **Minecraft** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//     })
//     }
//     else if(emoji.id == "590735635342557224") {
//         message.guild.fetchMember(user.id).then(member => {
//             let CSGO = message.guild.roles.find(n => n.name === "CSGO")
//             if(member.roles.has(CSGO.id)) {
//                 member.removeRole('631594631511015444')
//                 member.send("You have been removed from the **Minecraft** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if (!member.roles.has(CSGO.id)) {
//                 member.addRole('631594631511015444')
//                 member.send("You have been added to the **Minecraft** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "631980944592470016") {
//         message.guild.fetchMember(user.id).then(member => {
//             let Destiny2 = message.guild.roles.find(n => n.name === "Destiny 2")
//             if(member.roles.has(Destiny2.id)) {
//                 member.removeRole('631594989570490369')
//                 member.send("You have been removed from the **Destiny 2** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(Destiny2.id)) {
//                 member.addRole('631594989570490369')
//                 member.send("You have been added to the **Destiny 2** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "590734092056657961") {
//         message.guild.fetchMember(user.id).then(member => {
//             let ARK = message.guild.roles.find(n => n.name === "ARK")
//             if(member.roles.has(ARK.id)) {
//                 member.removeRole('631594922864279580')
//                 member.send("You have been removed from the **ARK** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(ARK.id)) {
//                 member.addRole('631594922864279580')
//                 member.send("You have been added to the **ARK** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "590735177504915486") {
//         message.guild.fetchMember(user.id).then(member => {
//             let RainbowSixSiege = message.guild.roles.find(n => n.name === "Rainbow Six Siege")
//             if(member.roles.has(RainbowSixSiege.id)) {
//                 member.removeRole('631594820833640459')
//                 member.send("You have been removed from the **Rainbow Six Siege** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(RainbowSixSiege.id)) {
//                 member.addRole('631594820833640459')
//                 member.send("You have been added to the **Rainbow Six Siege** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "634069975740448768") {
//         message.guild.fetchMember(user.id).then(member => {
//             let Atlas = message.guild.roles.find(n => n.name === "Atlas")
//             if(member.roles.has(Atlas.id)) {
//                 member.removeRole('634068988787294218')
//                 member.send("You have been removed from the **Atlas** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(Atlas.id)) {
//                 member.addRole('634068988787294218')
//                 member.send("You have been added to the **Atlas** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "634223792486350848") {
//         message.guild.fetchMember(user.id).then(member => {
//             let Fornite = message.guild.roles.find(n => n.name === "Fortnite")
//             if(member.roles.has(Fornite.id)) {
//                 member.removeRole('634204131816374292')
//                 member.send("You have been removed from the **Fortnite** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(Fornite.id)) {
//                 member.addRole('634204131816374292')
//                 member.send("You have been added to the **Fortnite** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "637867839822364673") {
//         message.guild.fetchMember(user.id).then(member => {
//             let Arma3 = message.guild.roles.find(n => n.name === "Arma 3")
//             if(member.roles.has(Arma3.id)) {
//                 member.removeRole('637867665603428362') 
//                 member.send('You have been removed from the **Arma 3** role.')
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(Arma3.id)) {
//                 member.addRole('637867665603428362') 
//                 member.send('You have been added to the **Arma 3** role.')
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "652047186657542155") {
//         message.guild.fetchMember(user.id).then(member => {
//             let DeadByDaylight = message.guild.roles.find(n => n.name === "Dead By Daylight")
//             if(member.roles.has(DeadByDaylight.id)) {
//                 member.removeRole('638239297543208971')
//                 member.send("You have been removed from the **Dead By Daylight** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(DeadByDaylight.id)) {
//                 member.addRole('638239297543208971')
//                 member.send("You have been added to the **Dead By Daylight** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id == "652346795372052510") {
//         message.guild.fetchMember(user.id).then(member => {
//         let HaloMasterChiefCollection = message.guild.roles.find(n => n.name === "Halo Master Chief Collection")
//         if(member.roles.has(HaloMasterChiefCollection.id)) {
//             member.removeRole('652345983337889812')
//             member.send("You have been removed from the **Halo Master Chief Collection** role.")
//             message.reactions.forEach(reaction => reaction.remove(member.id))
//         }
//         else if(!member.roles.has(HaloMasterChiefCollection.id)) {
//             member.addRole('652345983337889812')
//             member.send("You have been added to the **Halo Master Chief Collection** role.")
//             message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id === "662115779726016530") {
//         message.guild.fetchMember(user.id).then(member => {
//             let ModernWarfare = message.guild.roles.find(n => n.name === "Modern Warfare")
//             if(member.roles.has(ModernWarfare.id)) {
//                 member.removeRole('662112474883031040')
//                 member.send("You have been removed from the **Modern Warfare** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(!member.roles.has(ModernWarfare.id)) {
//                 member.addRole('662112474883031040')
//                 member.send("You have been added to the **Modern Warfare** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
//     else if(emoji.id === "662895709481205790") {
//         message.guild.fetchMember(user.id).then(member => {
//             let Dauntless = message.guild.roles.find(n => n.name === "Dauntless")
//             if(!member.roles.has(Dauntless.id)) {
//                 member.addRole('662771704635981855')
//                 member.send("You have been added to the **Dauntless** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//             else if(member.roles.has(Dauntless.id)) {
//                 member.removeRole('662771704635981855')
//                 member.send("You have been removed from the **Dauntless** role.")
//                 message.reactions.forEach(reaction => reaction.remove(member.id))
//             }
//         })
//     }
// });