const { RichEmbed } = require("discord.js")

module.exports = {
    name: 'bugs',
    category: 'info',
    description: 'Gives you all current bugs',
    run: async (client, message, args) => {
        let embed = new RichEmbed()
        .setColor(000000)
        .addField("**Bugs** ", `
        
        No current bugs!
        
        `)
        .setTimestamp(new Date())
        await message.delete()
        return message.channel.send(embed)
    }
}