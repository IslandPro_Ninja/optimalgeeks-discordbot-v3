const mysql = require("mysql")
const con = mysql.createConnection({
    host: "optimalgeeks.com",
    user: "ypswglmy_riley",
    password: "8Jn3P6Lki4",
    database: "ypswglmy_discord",
    port: '3306'
});

module.exports = {
    name: 'addall',
    category: 'xp',
    description: 'Adds users to database',
    run: async (client, message, args) => {
        let staffRole
        let target = message.mentions.users.first() || message.guild.members.get(args[0]) || message.author;
        let onlineMembers;
        let offlineMembers;
        let dndMembers;
        let idleMembers;

        let modRole = message.guild.roles.find(n => n.name === "Staff");
        if(!message.member.roles.has(modRole.id)) return message.reply("You do not have Role: **Staff**!")

        con.query(`SELECT * FROM xp`, async (err, rows) => {
            if(err) throw err;
            let guild = client.guilds.find(n => n.name === "Optimal Geeks Test Server (Offical)")
            onlineMembers = guild.members.filter(member => member.presence.status === "online");
            offlineMembers = guild.members.filter(member => member.presence.status === "offline");
            dndMembers = guild.members.filter(member => member.presence.status === "dnd");
            idleMembers = guild.members.filter(member => member.presence.status === "idle")
           
            if(onlineMembers) {
                await onlineMembers.forEach((member, key) => con.query(`INSERT INTO xp (id, xp) VALUES ('${member.id}', 0)`))
                if(offlineMembers) {
                    await offlineMembers.forEach((member, key) => con.query(`INSERT INTO xp (id, xp) VALUES ('${member.id}', 0)`))
                    if(dndMembers) {
                        await dndMembers.forEach((member, key) => con.query(`INSERT INTO xp (id, xp) VALUES ('${member.id}', 0)`))
                        if(idleMembers) {
                            await idleMembers.forEach((member, key) => con.query(`INSERT INTO xp (id, xp) VALUES ('${member.id}', 0)`))
                            await message.reply('You have added **everyone** to the database!')
                        }
                    }
                }
            }
            // await offlineMembers.forEach((member, key) => message.channel.send(member.id));
            // await onlineMembers.forEach((member, key) => message.channel.send(member.id))
            // con.query(`INSERT INTO xp (id, xp) VALUES ('${target.id}', 0)`)
            // message.channel.send(`You have added ${target} into the database!`)
        })
    }
}