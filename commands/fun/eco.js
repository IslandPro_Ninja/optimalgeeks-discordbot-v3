const { RichEmbed } = require("discord.js")

module.exports = {
    name: 'eco',
    aliases: ['say'],
    category: 'fun',
    description: `Eco's the text you say after the command`,
    run: async (client, message, args) => {
        if(!args[0]) return message.reply("Missing Args.");
        let text = args.splice(0).join(" ");
        await message.channel.send(text);  
        await message.delete();
    }
}