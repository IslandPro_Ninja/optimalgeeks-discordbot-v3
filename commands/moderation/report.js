const { RichEmbed } = require("discord.js")
const mysql = require("mysql")
const con = mysql.createConnection({
    host: "optimalgeeks.com",
    user: "ypswglmy_riley",
    password: "8Jn3P6Lki4",
    database: "ypswglmy_discord",
    port: '3306'
});

module.exports = {
    name: 'report',
    category: 'moderation',
    description: 'reports the user you specify',
    usage: '[mention reason]',
    run: async (client, message, args) => {
        con.query(`SELECT * FROM 244012247289036800_config`, async (err, rows) => 
        {
            let reason = args.splice(1).join(" ")
            let user = message.mentions.users.first();
            let id = rows[0].logschannelid
            let logChannel = message.guild.channels.get(id);
            if(message.mentions.users.size < 1)
                return message.reply("Give a valid user")
            if(reason.length < 0)
                return message.reply("Give a valid reason")
            await message.delete()
            let embed = new RichEmbed()
            .setColor(0x008040)
            .setAuthor(user.tag, user.avatarURL)
            .setThumbnail(user.avatarURL)
            .setTitle("Report")
            .addField("User:", `${user.username} || ID: ${user.id}`)
            .addField("Reason:", `${reason}`)
            .addField("Reporter:", `${message.author}`)
            .setTimestamp(new Date())
            .setFooter("Reported User", `${user.avatarURL}`)
            await logChannel.send({ embed })
        })
    }
}