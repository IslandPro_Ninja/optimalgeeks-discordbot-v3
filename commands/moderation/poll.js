const { RichEmbed } = require("discord.js")

module.exports = {
    name: 'poll',
    category: 'moderation',
    description: 'Lets you create a poll',
    run: async (client, message, args) => {
        let modRole = message.guild.roles.find(n => n.name === "Staff");

        if(!message.member.roles.find(r => r.name === "Staff")) return message.channel.send("You do not have the role: **Staff**");

        if(!args[0]) return message.channel.send(`Proper Usage: (prefix)poll Poll Question`);

        let embed = new RichEmbed()
        .setColor(0x0080ff)
        .setTitle(`New Poll Created Created By ` + message.author.username)
        .setFooter("React to this message to vote!")
        .setDescription(` 

        Question: ${args.splice(0).join(" ")}

        to vote yes react with ✅
        to vote no react with ⛔
        `)
        message.channel.send(embed).then(async pollmessage => {
            await pollmessage.react("✅")
            await pollmessage.react("⛔")
            await message.delete();
        })
    }
}