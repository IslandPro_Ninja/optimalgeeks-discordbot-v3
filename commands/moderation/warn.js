const { RichEmbed } = require('discord.js')
const { stringIndents } = require("common-tags")
const { promptMessage } = require("../../functions.js");
const mysql = require("mysql")
const con = mysql.createConnection({
    host: "optimalgeeks.com",
    user: "ypswglmy_riley",
    password: "8Jn3P6Lki4",
    database: "ypswglmy_discord",
    port: '3306'
});

module.exports = {
    name: "warn",
    category: "moderation",
    description: "Lets you warn users in the server",
    usage: "[ID | mention]",
    run: async (client, message, args) => {
        con.query(`SELECT * FROM 244012247289036800_config`, async (err, rows) =>
        {
            await message.delete()
            let modRole = message.guild.roles.find(n => n.name === "Staff");
            if(!message.member.roles.has(modRole.id)) return message.reply("You do not have Role: **Staff**!")
            let reason = args.splice(1).join(" ")
            let user = message.mentions.users.first();
            let id = rows[0].logschannelid;
            let logChannel = message.guild.channels.get(id);
            if (message.mentions.users.size <1) return message.reply("Give a valid user")
            if (reason.length < 1) return message.reply("Give a reason")
    
            let verifyEmbed = new RichEmbed()
            .setColor("GREEN")
            .setAuthor(`You have 30s to verify.`)
            .setDescription(`You want to warn ${user}.`)
    
            await message.channel.send(verifyEmbed).then(async msg => 
                {
                let emoji = await promptMessage(msg, message.author, 30, ["✅", "❌"])
    
                if(emoji === '✅') 
                {
                    msg.delete();
                    let embed = new RichEmbed()
                    .setColor(0x008040)
                    .setAuthor(user.tag, user.avatarURL)
                    .setThumbnail(user.avatarURL)
                    .setTitle("Warning")
                    .addField("User:", `${user.username} || ID: ${user.id}`)
                    .addField("Reason: ", `${reason}`)
                    .addField("Staff Member: ", `${message.author}`)
                    .setFooter('Warned User', `${user.avatarURL}`)
                    .setTimestamp(new Date())
                    await logChannel.send({ embed })
                    .catch(err => message.reply(`uh oh something broke... ${err}`));
                }
                else if (emoji === "❌") 
                {
                    await msg.delete();
                    await message.reply(`Warning cancelled`).then(m => m.delete(10000));
                }
            })
        })
        }
    }
