const { RichEmbed } = require("discord.js")

module.exports = {
    name: 'youtube',
    aliases: ["yt"],
    category: "social",
    description: 'Gives a link to OptimalGeeks youtube page',
    run: (client, message, args) => {
        let embed = new RichEmbed()
        .setColor(0xff0000)
        .addField("**Youtube** ", "https://www.youtube.com/channel/UC4KlsULR5Ld5wo1UJEEab-A?&ab_channel=OptimalGeeks")
        .setAuthor("Youtube", "https://yt3.ggpht.com/a-/AN66SAzYgZZS2TlGI__jJ9aK0u-jNWNAqlhjgv3D6Q=s900-mo-c-c0xffffffff-rj-k-no")
        .setTimestamp(new Date())
        return message.channel.send(embed)
    }
}