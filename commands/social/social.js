module.exports = {
    name: 'social',
    category: 'social',
    description: 'Gives a link to all of OptimalGeeks social media.',
    run: (client, message, args) => {
        let embed = new RichEmbed() 
        .setColor(0xff0000)
        .addField("**Twitter** ", "https://www.twitter.com/optimalgeeks")
        .addField("**Youtube** ", "https://www.youtube.com/channel/UC4KlsULR5Ld5wo1UJEEab-A")
        .addField("**Mixer** ", "https://mixer.com/optimalgeeks")
        .setTimestamp()
        return message.channel.send(embed)
    }
}