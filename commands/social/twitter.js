const { RichEmbed } = require("discord.js")

module.exports = {
    name: 'twitter',
    category: 'social',
    description: 'Gives a link to OptimalGeeks twitter page',
    run: (client, message, args) => {
        let embed = new RichEmbed()
        .setColor(0x0080ff)
        .addField("**Twitter** ", "https://www.twitter.com/optimalgeeks")
        .setAuthor("Twitter", "https://blog.addthiscdn.com/wp-content/uploads/2015/11/twitter.jpg")
        .setTimestamp(new Date())
        return message.channel.send(embed)
    }
}