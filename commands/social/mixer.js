const { RichEmbed } = require("discord.js")

module.exports = {
    name: 'mixer',
    category: 'social',
    description: 'Gives a link to Optimal Geeks mixer page.',
    run: async (client, message, args) => {
                let embed = new RichEmbed()
                .setColor(0x400080)
                .addField("**Mixer**", "https://mixer.com/optimalgeeks")
                .setTimestamp(new Date())
                await message.delete();
                return message.channel.send(embed);
    }
}