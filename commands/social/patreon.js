const { RichEmbed } = require("discord.js");

module.exports = {
    name: 'patreon',
    category: 'social',
    description: 'Gives a link to OptimalGeeks patreon page',
    run: (client, message, args) => {
        let embed = new RichEmbed()
        .setColor(0xff8000)
        .addField("**Patreon** ", "https://www.patreon.com/optimalgeeks")
        .setAuthor("Patreon", "https://s3.amazonaws.com/pas-wordpress-media/content/uploads/2015/03/Patreon-Logo.png")
        .setTimestamp(new Date())
        return message.channel.send(embed)
    }
}