// //Bot requirments\\
const discord = require("discord.js")
const client = new discord.Client(); 
const { config } = require("dotenv");
const fs = require("fs");
const mysql = require("mysql")
const con = mysql.createConnection({
    host: "optimalgeeks.com",
    user: "ypswglmy_riley",
    password: "8Jn3P6Lki4",
    database: "ypswglmy_discord",
    port: '3306'
});

con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
    let token = rows[0].token;
    client.login(token)
})

client.on('ready', (message) => {
    console.log(`logs.js is now loaded!`)
});

client.on('channelCreate', function(channel) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let id = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        if(channel.type === 'dm') return;
            var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor("OG-Logs", "https://cdn.discordapp.com/attachments/445097115119517699/647609055346556939/logo_1.png")
        .setDescription("**Channel Created**")
        .addField("Channel Name", ` ${channel.name} `, true)
        .addField("Channel Type", ` ${channel.type} `, true)
        .addField("Channel ID", `${channel.id}`, true)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})

client.on('channelDelete', function(channel) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor("OG-Logs", "https://cdn.discordapp.com/attachments/445097115119517699/647609055346556939/logo_1.png")
        .setDescription("**Channel Removed**")
        .addField("Channel Name", ` ${channel.name} `, true)
        .addField("Channel Type", ` ${channel.type} `, true)
        .addField("Channel ID", `${channel.id}`, true)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})

client.on('guildBanAdd', function(guild, user) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor(user.tag, user.avatarURL)
        .setThumbnail(`${user.avatarURL}`)
        .setDescription("**User Banned**")
        .setFooter(`User ID || ` + user.id)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})

client.on('guildBanRemove', function(guild, user) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor(user.tag, user.avatarURL)
        .setThumbnail(`${user.avatarURL}`)
        .setDescription("**User Unbanned**")
        .setFooter(`User ID || ` + user.id)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})

client.on('roleCreate', function(role) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor("OG-Logs", "https://cdn.discordapp.com/attachments/445097115119517699/647609055346556939/logo_1.png")
        .setDescription("**Role Created**")
        .addField("Role Name", ` ${role.name} `, true)
        .addField("Role ID", ` ${role.id} `, true)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})

client.on('roleDelete', function(role) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor("OG-Logs", "https://cdn.discordapp.com/attachments/445097115119517699/647609055346556939/logo_1.png")
        .setDescription("**Role Deleted**")
        .addField("Role Name", ` ${role.name} `, true)
        .addField("Role ID", `${role.id} `, true)
        .setTimestamp(new Date())
        logChannel.send( { embed })
    })
})

client.on('roleUpdate', function(role) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor("OG-Logs", "https://cdn.discordapp.com/attachments/445097115119517699/647609055346556939/logo_1.png")
        .setDescription("**Role Update**")
        .addField("Role Name", ` ${role.name} `, true)
        .addField("Permissions", ` ${role.permissions} `, true)
        .addField("Color", ` ${role.color} `, true)
        .addField("Hex Color", ` ${role.hexColor} `, true)
        .addField("Role Position", ` ${role.position} `, true)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})

client.on('messageDelete', function(message) {
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks Test Server (Offical)").channels.find(c => c.name === 'logs')
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor(message.author.tag, message.author.avatarURL)
        .setDescription("**Message Deleted**")
        .addField("Message", ` ${message.content} `, true)
        .setFooter(`User ID || ` + message.author.id)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})

client.on('messageUpdate', (oldMessage, newMessage) => {
    if(oldMessage.content === newMessage.content) {
        return;
    }
    con.query(`SELECT * FROM 244012247289036800_config`, (err, rows) => {
        let logschannelid = rows[0].logschannelid;
        let logChannel = client.guilds.find(n => n.name === "Optimal Geeks").channels.find(c => c.name === logs)
        var embed = new discord.RichEmbed()
        .setColor(0x008040)
        .setAuthor(oldMessage.author.tag, oldMessage.author.avatarURL)
        .setDescription(`Message Edited - [Jump To Message](${oldMessage.url})`)
        .addField("Old Message", `${oldMessage.content}`, true)
        .addField("New Message", `${newMessage.content}`, true)
        .setFooter(`User ID || ` + oldMessage.author.id)
        .setTimestamp(new Date())
        logChannel.send({ embed })
    })
})