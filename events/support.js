// const discord = require("discord.js");
// const client = new discord.Client();
// const config = require("./config.json");

// function clean(text) {
//     if (typeof(text) === "string")
//       return text.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
//     else
//         return text;
// }

// var prefix = config.prefix;
// var token = config.token;

// client.on("ready", () => {
//   console.log("support.js is now loaded!");
// });

// client.on("message", (message) => {
//   if (!message.content.startsWith(prefix) || message.author.bot) return;

//   if (message.content.toLowerCase().startsWith(prefix + `support`)) {
//     const embed = new discord.RichEmbed()
//     .setTitle(`:mailbox_with_mail: Support Help`)
//     .setColor(0xCF40FA)
//     .addField(`Tickets`, `[${prefix}new]() > Opens up a new ticket and tags our Staff Team\n[${prefix}close]() > Closes a ticket that has been resolved or been opened by accident`)
//     message.channel.send({ embed: embed });
//   }

//   if (message.content.toLowerCase().startsWith(prefix + `ping`)) {
//     message.channel.send(`Hoold on!`).then(m => {
//     m.edit(`:ping_pong: Wew, made it over the ~waves~ ! **Pong!**\nMessage edit time is ` + (m.createdTimestamp - message.createdTimestamp) + `ms, Discord API heartbeat is ` + Math.round(client.ping) + `ms.`);
//     });
// }

// if (message.content.toLowerCase().startsWith(prefix + `new`)) {
//     const reason = message.content.split(" ").slice(1).join(" ");
//     if (!message.guild.roles.find(n => n.name ===  "Staff")) return message.channel.send(`This server doesn't have a \`Support Team\` role made, so the ticket won't be opened.\nIf you are an administrator, make one with that name exactly and give it to users that should be able to see tickets.`);
//     if (message.guild.channels.find(n => n.name === "ticket-" + message.author.id)) return message.channel.send(`You already have a ticket open.`);
//     message.guild.createChannel(`ticket-${message.author.id}`, 'text').then(c => {
//         let category = message.guild.channels.find(c => c.name == "Support Tickets" && c.type == "category");
//         let role = message.guild.roles.find(n => n.name === "Staff");
//         let role2 = message.guild.roles.find(n => n.name === "@everyone");
//         c.setParent(category.id)
//         c.overwritePermissions(role, {
//             SEND_MESSAGES: true,
//             READ_MESSAGES: true
//         });
//         c.overwritePermissions(role2, {
//             SEND_MESSAGES: false,
//             READ_MESSAGES: false
//         });
//         c.overwritePermissions(message.author, {
//             SEND_MESSAGES: true,
//             READ_MESSAGES: true
//         });
//         message.channel.send(`:white_check_mark: Your ticket has been created, ` + message.guild.channels.get(c.id).toString());
//         const embed = new discord.RichEmbed()
//         .setColor(0xCF40FA)
//         .addField(`Hey ${message.author.username}!`, `Please try explain why you opened this ticket with as much detail as possible. Our **Staff Team** will be here soon to help.`)
//         .setTimestamp();
//         c.send({ embed: embed });
//     }).catch(console.error);
// }
// if (message.content.toLowerCase().startsWith(prefix + `close`)) {
//     if (!message.channel.name.startsWith(`ticket-`)) return message.channel.send(`You can't use the close command outside of a ticket channel.`);

//     message.channel.send(`Are you sure? Once confirmed, you cannot reverse this action!\nTo confirm, type \`-confirm\`. This will time out in 10 seconds and be cancelled.`)
//     .then((m) => {
//       message.channel.awaitMessages(response => response.content === '-confirm', {
//         max: 1,
//         time: 10000,
//         errors: ['time'],
//       })
//       .then((collected) => {
//           message.channel.delete();
//         })
//         .catch(() => {
//           m.edit('Ticket close timed out, the ticket was not closed.').then(m2 => {
//               m2.delete();
//           }, 3000);
//         });
//     });
// }

// });

// client.login(token);