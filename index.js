const { Client, Collection } = require("discord.js");
const { config } = require("dotenv");
const mysql = require("mysql")

const client = new Client({
    disableEveryone: true
})

const support = require('./unsorted/support')

const logs = require('./events/logs')
const rrgame = require('./unsorted/rrgame')
const rrverify = require('./unsorted/rrveryify')

// Collections
client.commands = new Collection();
client.aliases = new Collection();

config({
    path: __dirname + "/.env"
});

// Run the command loader
["command"].forEach(handler => {
    require(`./handlers/${handler}`)(client);
});

client.on("ready", () => {
    console.log(`Hi, ${client.user.username} is now online!`);

    client.user.setPresence({
        status: "online",
        game: {
            name: "over OptimalGeeks",
            type: "WATCHING"
        }
    }); 
})

client.on("message", async message => {
    con.query(`SELECT prefix FROM 244012247289036800_config`, async (err, rows) => {

        const prefix = rows[0].prefix;

        if (message.author.bot) return;
        if (!message.guild) return;
        if (!message.content.startsWith(prefix)) return;
        if (!message.member) message.member = await message.guild.fetchMember(message);
    
        const args = message.content.slice(prefix.length).trim().split(/ +/g);
        const cmd = args.shift().toLowerCase();
        
        if (cmd.length === 0) return;
        
        let command = client.commands.get(cmd);
        if (!command) command = client.commands.get(client.aliases.get(cmd));
    
        if (command) 
            command.run(client, message, args);
    })
});

const con = mysql.createConnection({
    host: "optimalgeeks.com",
    user: "ypswglmy_riley",
    password: "8Jn3P6Lki4",
    database: "ypswglmy_discord",
    port: '3306'
});

con.connect(err => {
    if(err) throw err;
    console.log("Connected to database!")
})

function generateXp() {
    let min = 1;
    let max = 1;

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

client.on('message', async message => {
    if(message.author.bot) return;
    if(message.channel.type === "dm") return;
    con.query(`SELECT prefix FROM 244012247289036800_config`, async (err, rows) => {
        let prefix = rows[0].prefix;
        const args = message.content.slice(prefix.length).trim().split(/ +/g);
        const cmd = args.shift().toLowerCase();
    
        if (cmd.length === 0) return;
    
        // Get the command
        let command = client.commands.get(cmd);
    
        if(!command) {
            var noxpRole = message.guild.roles.find(n => n.name === "No XP");
            if(message.member.roles.has(noxpRole.id)) return;
            con.query(`SELECT * FROM xp WHERE id = '${message.author.id}'`, (err, rows) => {
                if(err) throw err;
                
                let sql;
                let xp = rows[0].xp;
                if(rows.length < 1) {
                    sql = `INSERT INTO xp (id, xp) VALUES ('${message.author.id}', ${generateXp()})`
                }   
                if(xp === 50) {
                    message.reply(`You have reached 50 xp! You have been given the **Sweaty Scrubs** role!`)
                    message.member.addRole("631580279705698304")
                    sql = `UPDATE xp SET xp = ${xp + generateXp()} WHERE id = '${message.author.id}'`;
                }
                if(xp === 100) {
                    message.reply(`You have reached 100 xp! You have been given the **Geeks** role!`)
                    message.member.addRole("631580646560628767")
                    sql = `UPDATE xp SET xp = ${xp + generateXp()} WHERE id = '${message.author.id}'`;
                }
                else{
                    let xp = rows[0].xp;
                    sql = `UPDATE xp SET xp = ${xp + generateXp()} WHERE id = '${message.author.id}'`;
                }
                con.query(sql);
            })
    }
    })
})

client.login(process.env.TOKEN);